<?php
/* Copyright 2019, 2020 Romain "Artefact2" Dal Maso <romain.dalmaso@artefact2.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

$c = make_curl('https://www.ssga.com/bin/v1/ssmp/fund/fundfinder?country=uk&language=en_gb&role=institutional&product=etfs&ui=fund-finder');
$res = json_decode(curl_exec_cached($c), true);
assert(is_array($res));
assert($res['msg'] === 'success');
assert($res['status'] === 200);

$data = [ 'exposures' => [] ];
$funds = $res['data']['funds']['etfs']['datas'];
shuffle($funds);
$i = 0;
$count = count($funds);
foreach($funds as $fund) {
	progress(++$i, $count, "%s", $fund['fundName']);

	if(!preg_match('%^SPDR([^ ]*) (?<index>.+?) UCITS ETF( \((Acc|Dist)\))?$%', $fund['fundName'], $m)) {
		error("%s: bad fund name?", $fund['fundName']);
		continue;
	}

	// /uk/en_gb/institutional/etfs/funds/spdr-sp-500-esg-screened-ucits-etf-acc-sppy-gy
	// /uk/en_gb/institutional/etfs/library-content/products/fund-data/etfs/emea/holdings-daily-emea-en-sppy-gy.xlsx
	$uri = sprintf(
		'https://www.ssga.com/uk/en_gb/institutional/etfs/library-content/products/fund-data/etfs/emea/holdings-daily-emea-en-%s.xlsx',
		implode('-', array_slice(explode('-', $fund['fundUri']), -2))
	);

	/* XXX: ssconvert(1) doesn't like pipes */
	$c = make_curl($uri);
	curl_setopt($c, CURLOPT_FOLLOWLOCATION, true);
	file_put_contents($fund['fundTicker'].'.xlsx', curl_exec_cached($c));

	passthru('ssconvert -T Gnumeric_stf:stf_csv '.escapeshellarg($fund['fundTicker']).'.xlsx', $ret);
	assert($ret === 0);
	$csv = explode("\n", file_get_contents($fund['fundTicker'].'.csv'));
	@unlink($fund['fundTicker'].'.xlsx');
	@unlink($fund['fundTicker'].'.csv');
	assert(is_array($csv));

	foreach($csv as &$l) {
		$l = explode(",", $l);
		foreach($l as &$v) {
			if($v !== "" && $v[0] === '"') $v = substr($v, 1, -1);
		}
		unset($v);
	}
	unset($l);

	$exposures = [
		'type' => [],
		'gics' => [],
		'country' => [],
		'currency' => [],
	];

	$cols = array_flip($csv[5]);
	if(!isset($cols['Percent of Fund'])) {
		error("%s: no percentages", $fund['fundName']);
		continue;
	}
	foreach($csv as $l) {
		if(!preg_match('%^[A-Z]{2}[A-Z0-9]{9}[0-9]$%', $l[0])) continue;

		$weight = floatval($l[$cols['Percent of Fund']]);

		if(isset($cols['Maturity Date']) && $l[$cols['Maturity Date']] !== "") {
			$type = 'Fixed Income';
			$cur = $l[$cols['Currency Local']];
			$country = $l[$cols['Country of Issue']];
			$gics = 'Unknown'; /* XXX */
		} else if(isset($cols['Sector Classification']) && $l[$cols['Sector Classification']] !== "") {
			$type = 'Equity';
			$cur = $l[$cols['Currency']];
			$country = $l[$cols['Trade Country Name']];
			$gics = $l[$cols['Sector Classification']];
		} else {
			continue;
		}

		$exposures['type'][$type] = ($exposures['type'][$type] ?? 0.0) + $weight;
		$exposures['currency'][$cur] = ($exposures['currency'][$cur] ?? 0.0) + $weight;
		$exposures['country'][$country] = ($exposures['country'][$country] ?? 0.0) + $weight;
		$exposures['gics'][$gics] = ($exposures['gics'][$gics] ?? 0.0) + $weight;
	}

	/* XXX: currency hedged funds */
	if(preg_match('% (?<cur>[A-Z]{3}) Hdg$%', $m['index'], $ch) || preg_match('% Hdg to (?<cur>[A-Z]{3})$%', $m['index'], $ch)) {
		$exposures['currency'] = [
			$ch['cur'] => 100.0,
		];
	}

	$data['exposures'][$m['index']] = $exposures;
}

return $data;
