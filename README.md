`destriant-ext` contains scripts to generate the static data for
[Destriant](https://gitlab.com/artefact2/destriant).

Released under the Apache License 2.0.
