<?php
/* Copyright 2019, 2020 Romain "Artefact2" Dal Maso <romain.dalmaso@artefact2.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

$c = make_curl('https://www.ishares.com/uk/professional/en/product-screener/product-screener-v3.jsn?dcrPath=/templatedata/config/product-screener-v3/data/en/uk/product-screener/product-screener');
curl_setopt($c, CURLOPT_HTTPHEADER, [
	'Referer: https://www.ishares.com/',
	'Cookie: blkUserType-ishares-uk=institutional; s_cc=true;',
]);
$res = json_decode(curl_exec_cached($c), true);
assert(is_array($res));
assert($res['code'] === 200);
assert($res['message'] === 'OK');
assert($res['status'] === 'success');



$columns = [];
foreach($res['data']['tableData']['columns'] as $k => $col) {
	$columns[$col['name']] = $k;
}
assert($columns['productPageUrl'] > 0);
assert($columns['fundName'] > 0);

$data = [];
shuffle($res['data']['tableData']['data']);
$i = 0;
$count = count($res['data']['tableData']['data']);
foreach($res['data']['tableData']['data'] as $fund) {
	progress(++$i, $count, "%s", $fund[$columns['fundName']]);
	if(!preg_match('/(^|\s)ETF(\s|$)/', $fund[$columns['fundName']])) continue;
	$ret = fetch_fund('https://www.ishares.com'.str_replace('+', '%20', $fund[$columns['productPageUrl']]).'?siteEntryPassthrough=true');
	if(is_array($ret)) {
		$data['exposures'][$ret[0]] = $ret[1];
	}
}
return $data;

function fetch_fund(string $uri): ?array {
	$c = make_curl($uri);
	$html = curl_exec_cached($c);

	if(strpos($html, "\nProduct Structure\n") !== false) {
		$structure = explode("\nProduct Structure\n", $html, 2)[1];
		$structure = explode("\n</div>\n", $structure, 2)[0];
		assert(preg_match('/<span class="data">\n(?<structure>[^\n]+)\n<\/span>/', $structure, $m));
		if($m['structure'] !== "Physical") return null;
	}

	$benchmark = explode("\nBenchmark Index\n", $html, 2);
	if(count($benchmark) !== 2) {
		error("%s: no benchmark", $uri);
		return null;
	}
	$benchmark = explode("\n</div>\n", $benchmark[1], 2)[0];
	assert(preg_match('/<span class="data">\n(?<benchmark>[^\n]+)\n<\/span>/', $benchmark, $m));
	$benchmark = $m['benchmark'];
	assert($benchmark !== '' && trim($benchmark) === $benchmark);

	assert(preg_match('/<div id="allHoldingsTab" data-ajaxUri="(?<uri>[^"]+)"/', $html, $m));
	$c = make_curl('https://www.ishares.com'.$m['uri']);
	$txt = substr(curl_exec_cached($c), 3); /* Remove UTF-8 BOM, it throws off json_decode() */
	$holdings = json_decode($txt, true);
	assert(is_array($holdings));
	assert(is_array($holdings['aaData']));

	$prices = [];
	foreach($holdings['aaData'] as $sec) {
		if($sec[2] !== 'Cash') continue;
		$prices[$sec[0]] = $sec[4]['raw'];
	}

	$exposures = [];
	switch(count($holdings['aaData'][0])) {
	case 13:
		$cols = [ 'type' => 2, 'gics' => 8, 'country' => 11, 'currency' => 12 ];
		break;
	case 14:
		$cols = [ 'type' => 2, 'gics' => 9, 'country' => 12, 'currency' => 13 ];
		break;
	case 15:
		$cols = [ 'type' => 2, 'gics' => 10, 'country' => 13, 'currency' => 14 ];
		break;
	case 16:
		$cols = [ 'type' => 2, 'gics' => 8, 'country' => 13, 'currency' => 14 ];
		break;
	default:
		error("%s: unknown column types", $benchmark);
		return null;
	}
	foreach($holdings['aaData'] as $sec) {
		if($sec[2] === 'Forwards' || $sec[2] === 'FX') {
			assert($sec[0] === substr($sec[1], 0, 3));
			assert(preg_match('/^[A-Z]{3}\/[A-Z]{3}$/', $sec[1]));
			list($from, $to) = explode('/', $sec[1]);
			$amount = $sec[5]['raw'] * $prices[$from] / $prices[$to];
			$exposures['currency'][$from] = ($exposures['currency'][$from] ?? 0) + $amount;
			$exposures['currency'][$to] = ($exposures['currency'][$to] ?? 0) - $amount;
			continue;
		}

		foreach($cols as $key => $idx) {
			$v = $sec[$idx];
			$exposures[$key][$v] = ($exposures[$key][$v] ?? 0) + $sec[7]['raw'];
		}
	}

	foreach($exposures as $type => &$e) {
		if(isset($e['-'])) {
			$e['N/A'] = $e['-'];
			unset($e['-']);
		}
	}
	unset($e);

	if(preg_match('/ Index$/', $benchmark)) {
		$benchmark = substr($benchmark, 0, -6);
	}
	return [ $benchmark, $exposures ];
}
